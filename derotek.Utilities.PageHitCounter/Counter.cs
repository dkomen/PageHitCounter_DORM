﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Utilities.PageHitCounter
{
    public class Counter
    {
        private static DORM.ConcreteAccessors.FluentNhibernate.Accessor<Entities.PageHit> _dal = new DORM.ConcreteAccessors.FluentNhibernate.Accessor<Entities.PageHit>("Data Source=localhost,1433;Initial Catalog=Gys;integrated security=true", DORM.Enumerations.DatastoreType.MsSql);

        private Counter()
        {
            _dal.CreateSessionFactory(true, "Data Source=localhost,1433;Initial Catalog=Gys;integrated security=true", DORM.Enumerations.DatastoreType.MsSql);
        }
        public static void AddHit(string SystemId, DateTime timeSpamp, string page, string refererId)
        {
            Entities.PageHit pageHit = new Entities.PageHit() {SystemId=SystemId, TimeStamp=timeSpamp, Page=page, RefererId=refererId };
            AddHit(pageHit);
        }

        public static void AddHit(Entities.PageHit pageHit)
        {
            System.Threading.Tasks.Task t = new System.Threading.Tasks.Task(() => 
            {
                using (DORM.Interfaces.ITransaction trans = _dal.TransactionCreate(true))
                {
                    trans.AddOrUpdate(pageHit);
                    trans.Commit();
                }
            }
            );
            t.Start();
        }
    }   
}
