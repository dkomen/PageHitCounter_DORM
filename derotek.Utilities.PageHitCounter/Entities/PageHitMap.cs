﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Mapping;

namespace derotek.Utilities.PageHitCounter.Entities
{
    public class PageHitMap: DORM.ConcreteAccessors.FluentNhibernate.EntityMap<PageHit>
    {
        public PageHitMap()
        {
            Map(x => x.TimeStamp).Not.Nullable().Index("Idx_PageHit_SystemId_TimeStamp");
            Map(x => x.SystemId).Not.Nullable().Length(25).Index("Idx_PageHit_SystemId_TimeStamp");
            Map(x => x.Page).Not.Nullable().Length(256);
            Map(x => x.RefererId).Not.Nullable().Length(60);
        }
    }
}
