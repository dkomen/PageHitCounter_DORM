﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Utilities.PageHitCounter.Entities
{
    public class PageHit: DORM.Entity
    {
        public virtual DateTime TimeStamp { get; set; }
        public virtual string SystemId { get; set; }
        public virtual string Page { get; set; }
        public virtual string RefererId { get; set; }
    }
}
